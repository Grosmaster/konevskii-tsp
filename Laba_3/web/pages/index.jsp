<%@page import="folder.Main"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="./css/indexcss.css">
        <title>login</title>
    </head>
    <body>        
        <div class = "container">   
            <header>
                <img src="./images/logo.png">
            </header>
            <div>
                <h3>Для входа на сервис введите личные данные</h3>
                <form name="username" action="pages/user.jsp" method="POST">
                    <label> Телефон <input class="phone" type="text" name="phone" value="" size="20" /></label><br>
                    <label> Пароль <input class=" password" type="password" name="password" value="" size="20" /></label><br>
                    <input class="button" type="submit" value="Войти" />
                </form>
            </div> 
            <div class="boot">
                <a href = "pages/createUser.jsp">Создать аккаунт</a>
                <a href = "pages/loginAdmin.jsp">Админ</a>  
            </div>

        </div>            
    </body>
</html>
