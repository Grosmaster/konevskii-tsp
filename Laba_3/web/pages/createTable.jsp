<%-- 
    Document   : createTable
    Created on : 17.04.2017, 15:48:13
    Author     : Vladisalv
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="../css/indexcss.css">
        <title>readXML</title>
    </head>
    <body>
        <jsp:useBean id = "obj" class="ejb.StreamXMLBean"></jsp:useBean>
            <div>
                <header>
                    <img src="../images/logo.png">     
                </header>
                <div class="boot">
                <% obj.read(request.getParameter("file"), request.getParameter("name"));%>
                <h2><a href = "admin.jsp">XML считан</a></h2>
            </div>
        </div>
    </body>
</html>
