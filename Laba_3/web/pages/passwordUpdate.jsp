<%-- 
    Document   : passwordUpdate
    Created on : 22.04.2017, 19:47:05
    Author     : Vladisalv
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="../css/servicecss.css">
        <title>password</title>
    </head>
    <body>
        <div class="container">
            <header>
                <img src="../images/logo.png">
            </header>
            <div>
                <h3>Смена пароля</h3>
       
                <form name="tv" action="newPassword.jsp" method="POST">                  
                    <label> Новый пароль <input type="text" class="info" name="password" value="" size="20" /></label><br>
                    <input class="button" type="submit" value="Сменить" />
                </form>
            </div>
            <div class = "boot">
                <a href = "user.jsp">Назад</a>  
            </div>
        </div>        
    </body>
</html>
