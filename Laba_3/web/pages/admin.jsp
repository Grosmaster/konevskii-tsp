<%-- 
    Document   : admin
    Created on : 04.04.2017, 22:37:19
    Author     : Vladisalv
--%>

<%@page import="model.dto.OrderDto"%>
<%@page import="java.util.ArrayList"%>
<%@page import="model.dto.ServiceDto"%>
<%@page import="java.util.List"%>
<%@page import="controllers.AdminController"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="../css/windowcss.css">
        <title>admin</title>
    </head>
    <body>
        <div class = "container">
            <header>
                <img src="../images/logo.png">   
            </header>
            <div>
                <% if (session.getAttribute("password") == null) {
                        session.setAttribute("password", request.getParameter("password"));
                    } %>
                <% if (AdminController.login((String) session.getAttribute("password"))) { %>
                <h2> Admin </h2>
                <a href = "login.jsp">Выйти</a><br>
                <a href = "userList.jsp">Список пользователей</a>
                <a href = "writeXML.jsp">записать XML</a>
                <a href = "readXML.jsp">считать XML</a>
                <form name="number" action="activService.jsp" method="POST">                
                    <label> Номер заявки <input type="text" name="number" value="" size="20" /></label>
                    <input type="submit" value="выбрать" />
                </form>
                <% List<OrderDto> list = AdminController.orderList();%>
                <table border = "2">
                    <thead>
                        <tr>
                            <th>Номер</th>
                            <th>Пользователь</th>
                            <th>Услуга</th>
                            <th>Описание</th>
                            <th>Статус</th>
                        </tr>
                    </thead>

                    <% for (int i = 0; i < list.size(); i++) {%>
                    <tr>
                        <th> <%= list.get(i).getNumber()%> </th>
                        <th> <%= list.get(i).getPhone()%> </th>
                        <th> <%= list.get(i).getName()%> </th>
                        <th><%= list.get(i).getInfo()%></th>
                        <th> <%= list.get(i).getStatus()%> </th>                      
                    </tr>
                    <%}%>
                </table>
                <%} else { %>
                <h1> Введены не корректные данные </h1>
                <a href = "login.jsp">Назад</a> 
                <%}%>
            </div>
        </div>




    </body>
</html>
