<%-- 
    Document   : user
    Created on : 04.04.2017, 22:17:59
    Author     : Vladisalv
--%>

<%@page import="java.util.List"%>
<%@page import="model.dto.ServiceDto"%>
<%@page import="java.lang.String"%>
<%@page import="controllers.UserController"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="../css/windowcss.css">
        <title>user</title>
    </head>
    <body>
        <div class="container">
            <header>
                <img src="../images/logo.png">    
            </header>
            <div>
                <% if (session.getAttribute("phone") == null) {
                        session.setAttribute("phone", request.getParameter("phone"));
                    } %>
                <% if (session.getAttribute("password") == null) {
                        session.setAttribute("password", request.getParameter("password"));
                    } %>
                <% if (UserController.login((String) session.getAttribute("phone"), (String) session.getAttribute("password"))) {%>
                <a href = "login.jsp">Выйти</a>
                <a href = "passwordUpdate.jsp">Сменить пароль</a>
                <h2> Телефон:  <%=(String) session.getAttribute("phone")%> </h2>
                <% List<ServiceDto> list = UserController.serviceList((String) session.getAttribute("phone"));%>
                <table border = "2">
                    <thead>
                        <tr>
                            <th>Услуга</th>
                            <th>Описание</th>
                        </tr>
                    </thead>
                    <% for (int i = 0; i < list.size(); i++) {%>
                    <tr>
                        <th> <%= list.get(i).getName()%> </th>
                        <th><%= list.get(i).getInfo()%></th>
                    </tr>
                    <%}%>
                </table>
                <p>Подключить:</p>
                <ul> 
                    <li><a href = "servicTV.jsp">ТВ</a></li>
                    <li><a href = "servicPhone.jsp">Тариф</a></li>
                    <li><a href = "servicNetwork.jsp">Интернет</a></li>
                </ul>

                <% if (request.getParameter("nameService") != null) {
                        UserController.creatOrder((String) session.getAttribute("phone"), request.getParameter("nameService"), request.getParameter("info"));
                    };%>

                <%} else { %>
                <h1> Введены не корректные данные </h1>
                <a href = "login.jsp">Назад</a> 
                <%}%>
            </div>         
        </div>
    </body>
</html>
