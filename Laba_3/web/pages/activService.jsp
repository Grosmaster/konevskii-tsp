<%-- 
    Document   : activService
    Created on : 05.04.2017, 3:52:00
    Author     : Vladisalv
--%>

<%@page import="model.dto.OrderDto"%>
<%@page import="controllers.AdminController"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="../css/windowcss.css">
        <title>activService</title>
    </head>
    <body>
        <div class = "container">
            <header>
                <img src="../images/logo.png">   
            </header>
            <div>
                <h2>Заявка</h2>
                <% if (AdminController.login((String) session.getAttribute("password"))) { %>
                <% session.setAttribute("number", request.getParameter("number"));%>
                <h2> Номер: <%= request.getParameter("number")%></h2>
                <% OrderDto order = AdminController.order(Integer.parseInt(request.getParameter("number")));%>
                <p>Телефон: <%=  order.getPhone()%></p>
                <p>Описание: <%= order.getName() + " - " + order.getInfo()%></p>
                <form name="phone" action="add.jsp" method="POST">     
                    <input class="button" type="submit" value="Принять" />
                </form>
                <form name="phone" action="sub.jsp" method="POST">                
                    <input class="button" type="submit" value="Отклонить" />
                </form>
                <%} else { %>
                <h1> Введены не корректные данные </h1>
                <a href = "http://localhost:8080/Laba_3">Назад</a> 
                <%}%>
            </div>
        </div>
        

    </body>
</html>
