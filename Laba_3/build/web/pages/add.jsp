<%-- 
    Document   : add
    Created on : 05.04.2017, 5:37:36
    Author     : Vladisalv
--%>

<%@page import="controllers.AdminController"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="../css/indexcss.css">
        <title>add</title>
    </head>
    <body>
        <div>
            <header>
                <img src="../images/logo.png">     
            </header>
            <div class="boot">
                <% AdminController.addOrder(Integer.parseInt((String) session.getAttribute("number")));%>
                <h2><a href = "admin.jsp">Заявка принята</a></h2>
            </div>
        </div>
    </body>
</html>
