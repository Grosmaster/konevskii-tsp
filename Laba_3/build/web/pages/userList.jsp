<%-- 
    Document   : userList
    Created on : 05.04.2017, 3:48:10
    Author     : Vladisalv
--%>

<%@page import="java.util.List"%>
<%@page import="controllers.AdminController"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="../css/windowcss.css">
        <title>userList</title>
    </head>
    <body>
        <div class = "container">
            <header>
                <img src="../images/logo.png">   
            </header>
            <div>
                <h2>Список пользователей</h2>
                <% if (AdminController.login((String) session.getAttribute("password"))) { %>
                <form name="phone" action="info.jsp" method="POST">                
                    <label> Номер телефона <input type="text" name="phone" value="" size="20" /></label>
                    <input type="submit" value="выбрать" />
                </form>
                <%List<String> list = AdminController.phones();%>
                <table border = "2">
                    <thead>
                        <tr>
                            <th>Телефон</th>
                        </tr>
                    </thead>

                    <% for (int i = 0; i < list.size(); i++) {%>
                    <tr>
                        <th> <%= list.get(i)%> </th>
                    </tr>
                    <%}%>
                    <%} else { %>
                    <h1> Введены не корректные данные </h1>
                    <a href = "http://localhost:8080/Laba_3">Назад</a> 
                    <%}%>
                </table>
            </div>
        </div>

    </body>
</html>
