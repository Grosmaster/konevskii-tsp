<%-- 
    Document   : info
    Created on : 05.04.2017, 4:15:44
    Author     : Vladisalv
--%>

<%@page import="controllers.UserController"%>
<%@page import="java.util.List"%>
<%@page import="model.dto.ServiceDto"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="../css/windowcss.css">
        <title>JSP Page</title>
    </head>
    <body>
        <div class="container">
            <header>
                <img src="../images/logo.png">   
            </header>
            <div>
                <h2>Информация о пользователе</h2>

                <% List<ServiceDto> list = UserController.serviceList(request.getParameter("phone"));%>
                <h2>Телефон: <%=request.getParameter("phone")%></h2>
                <table border = "2">
                    <thead>
                        <tr>
                            <th>Услуга</th>
                            <th>Описание</th>
                        </tr>
                    </thead>

                    <% for (int i = 0; i < list.size(); i++) {%>
                    <tr>
                        <th> <%= list.get(i).getName()%> </th>
                        <th><%= list.get(i).getInfo()%></th>
                    </tr>
                    <%}%>
                </table>
                <a href = "admin.jsp">Назад</a>
            </div>
        </div>

    </body>
</html>
