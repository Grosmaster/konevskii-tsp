<%-- 
    Document   : writeXML
    Created on : 16.04.2017, 23:30:52
    Author     : Vladisalv
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="../css/indexcss.css">
        <title>write</title>
    </head>
    <body>
        <div class = "container">   
            <header>
                <img src="../images/logo.png">
            </header>
            <div>
                <h3>Записать XML</h3>
                <form name="formxml" action="createXML.jsp" method="POST">
                    <select name ="name">
                        <option value="user">user</option>
                        <option value="order">order</option>
                        <option value="service">service</option>
                    </select><br>

                    <label> Телефон <input class=" phone" type="text" name="phone" value="" size="20" /></label><br>

                    <input class="button" type="submit" value="Выполнить" />
                </form>
            </div> 
        </div>            
    </body>
</html>
