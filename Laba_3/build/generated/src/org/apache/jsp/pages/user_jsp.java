package org.apache.jsp.pages;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.List;
import model.dto.Service;
import java.lang.String;
import controllers.UserController;

public final class user_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>user</title>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        ");
 if(session.getAttribute("phone") == null)session.setAttribute("phone",request.getParameter("phone") ); 
      out.write("\n");
      out.write("        ");
 if(session.getAttribute("password") == null)session.setAttribute("password",request.getParameter("password") ); 
      out.write("\n");
      out.write("        ");
 if(UserController.login((String) session.getAttribute("phone"),(String) session.getAttribute("password"))) { 
      out.write("\n");
      out.write("       \n");
      out.write("        <h1> ");
      out.print((String) session.getAttribute("phone"));
      out.write(" </h1>\n");
      out.write("            ");
 List<Service> list = UserController.serviceList((String)session.getAttribute("phone"));
      out.write("\n");
      out.write("            <table border = \"2\">\n");
      out.write("                <tr>\n");
      out.write("                    <th>Услуга</th>\n");
      out.write("                    <th>Описание</th>\n");
      out.write("                </tr>\n");
      out.write("                ");
 for (int i = 0; i < list.size(); i++) { 
      out.write("\n");
      out.write("                    <tr>\n");
      out.write("                        <th> ");
      out.print( list.get(i).getName() );
      out.write(" </th>\n");
      out.write("                        <th>");
      out.print( list.get(i).getInfo());
      out.write("</th>\n");
      out.write("                    </tr>\n");
      out.write("                ");
}
      out.write("\n");
      out.write("            </table>\n");
      out.write("            <p>Подключить:</p>\n");
      out.write("            <ul> \n");
      out.write("                <li><a href = \"servicTV.jsp\">ТВ</a></li>\n");
      out.write("                <li><a href = \"servicPhone.jsp\">Тариф</a></li>\n");
      out.write("                <li><a href = \"servicNetwork.jsp\">Интернет</a></li>\n");
      out.write("            </ul>\n");
      out.write("            \n");
      out.write("            ");
 if(request.getParameter("nameService") != null) {UserController.creatOrder((String) session.getAttribute("phone"), request.getParameter("nameService"), request.getParameter("info"));} ;
      out.write("\n");
      out.write("    \n");
      out.write("        ");
} else { 
      out.write("\n");
      out.write("         <h1> Введены не корректные данные </h1>\n");
      out.write("         <a href = \"http://localhost:8080/Laba_3\">Назад</a> \n");
      out.write("        ");
}
      out.write("\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
