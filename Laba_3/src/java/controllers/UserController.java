/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.util.List;
import model.dao.OrderDao;
import model.dao.ServiceDao;
import model.dao.UserDao;
import model.dao.hibernate.OrderHibernateDao;
import model.dao.hibernate.ServiceHibernateDao;
import model.dao.hibernate.UserHibernateDao;
import model.dao.mySqlImplemets.OrderMySqlDao;
import model.dao.mySqlImplemets.ServiceMySqlDao;
import model.dao.mySqlImplemets.UserMySqlDao;
import model.dto.ServiceDto;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author Vladisalv
 */
public class UserController {

    public static boolean login(String phone, String password) {
        //UserDao dao = new UserMySqlDao();       
        UserDao dao = new UserHibernateDao();        
        return dao.loginUser(Integer.parseInt(phone)).equals(hashPassword(password));
    }

    public static List<ServiceDto> serviceList(String phone) {
        //ServiceDao dao = new ServiceMySqlDao();
        ServiceDao dao = new ServiceHibernateDao();
        return dao.services(Integer.parseInt(phone));
    }

    public static void creatOrder(String phone, String name, String info) {
        //OrderDao dao = new OrderMySqlDao();
        OrderDao dao = new OrderHibernateDao();
        dao.orderAdd(Integer.parseInt(phone), name, info, "PROCESSING");
    }

    public static void creatUser(String phone, String password) {
        //UserDao dao = new UserMySqlDao();
        UserDao dao = new UserHibernateDao();
        dao.createUser(Integer.parseInt(phone), hashPassword(password));
    }

    public static void updatePassword(String phone, String password) {
        //UserDao dao = new UserMySqlDao();
        UserDao dao = new UserHibernateDao();
        dao.updatePassword(phone, hashPassword(password));
    }
    
    private static String hashPassword(String password){
        StringBuffer code = new StringBuffer(); //the hash code
        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        byte bytes[] = password.getBytes();
        byte digest[] = messageDigest.digest(bytes); //create code
        for (int i = 0; i < digest.length; ++i) {
            code.append(Integer.toHexString(0x0100 + (digest[i] & 0x00FF)).substring(1));
        }
        return code.toString();
    }
}
