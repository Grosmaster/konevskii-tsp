/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.util.ArrayList;
import java.util.List;
import model.dao.OrderDao;
import model.dao.ServiceDao;
import model.dao.UserDao;
import model.dao.hibernate.OrderHibernateDao;
import model.dao.hibernate.ServiceHibernateDao;
import model.dao.hibernate.UserHibernateDao;
import model.dao.mySqlImplemets.OrderMySqlDao;
import model.dao.mySqlImplemets.ServiceMySqlDao;
import model.dao.mySqlImplemets.UserMySqlDao;
import model.dto.OrderDto;

/**
 *
 * @author Vladisalv
 */
public class AdminController {

    public static boolean login(String password) {
        return "1".equals(password);
    }

    public static List<OrderDto> orderList() {
        //OrderDao dao = new OrderMySqlDao();
        OrderDao dao = new OrderHibernateDao();
        return dao.orders();
    }

    public static OrderDto order(int number) {
        //OrderDao dao = new OrderMySqlDao();
        OrderDao dao = new OrderHibernateDao();
        return dao.order(number);
    }

    public static void addOrder(int number) {
        //OrderDao dao = new OrderMySqlDao();
        OrderDao dao = new OrderHibernateDao();
        //ServiceDao serv = new ServiceMySqlDao();
        ServiceDao serv = new ServiceHibernateDao();
        OrderDto order = dao.order(number);
        serv.servicAdd(Integer.parseInt(order.getPhone()), order.getName(), order.getInfo());
        dao.orderUpdate(number, "ACCEPTED");
    }

    public static void subOrder(int number) {
        //OrderDao dao = new OrderMySqlDao();
        OrderDao dao = new OrderHibernateDao();
        dao.orderUpdate(number, "RESECTED");
    }

    public static List<String> phones() {
        //UserDao dao = new UserMySqlDao();
        UserDao dao = new UserHibernateDao();
        List<String> list = dao.phones();
        return list;
    }
}
