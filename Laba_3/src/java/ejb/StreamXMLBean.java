package ejb;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import javax.ejb.Stateless;

import jaxb.UserJAXB;

import java.io.File;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import jaxb.ListOrderJAXB;
import jaxb.ListServiceJAXB;
import jaxb.OrderJAXB;
import jaxb.ServiceJAXB;


@Stateless
public class StreamXMLBean implements StreamXMLBeanLocal {

    @Override
    public String write(String name, String phone) {
        
        if (name.equals("user")) {
            String password = "";
            try {
                InitialContext ic = new InitialContext();
                DataSource ds = (DataSource) ic.lookup("jdbc/laba_3");
                Connection conn = ds.getConnection();
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery("select * from laba_3.user where " + " phone = " + phone);
                while (rs.next()) {
                    //с 1 до n
                    password = rs.getString(2);
                }

            } catch (Exception ex) {

            }
            UserJAXB user = new UserJAXB(phone, password);
            try {
                
                JAXBContext context = JAXBContext.newInstance(UserJAXB.class);
                Marshaller marshaller = context.createMarshaller();                
                StringWriter sw = new StringWriter();                
                marshaller.marshal(user, sw);                
                return sw.toString();
            } catch (JAXBException ex) {
                Logger.getLogger(StreamXMLBean.class.getName())
                        .log(Level.SEVERE, null, ex);
            }
        }
        //______________________________________________________________________
        if (name.equals("service")) {
            ListServiceJAXB lsjaxb = new ListServiceJAXB();
            try {
                InitialContext ic = new InitialContext();
                DataSource ds = (DataSource) ic.lookup("jdbc/laba_3");
                Connection conn = ds.getConnection();
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery("select * from laba_3.service where " + " phone = " + phone);

                while (rs.next()) {
                    //с 1 до n id phone name info
                    lsjaxb.add(new ServiceJAXB(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4)));
                }

            } catch (Exception ex) {

            }
            try {
                
                JAXBContext context = JAXBContext.newInstance(ListServiceJAXB.class);
                Marshaller marshaller = context.createMarshaller();
                StringWriter sw = new StringWriter();                
                marshaller.marshal(lsjaxb, sw);                
                return sw.toString();
            } catch (JAXBException ex) {
                Logger.getLogger(StreamXMLBean.class.getName())
                        .log(Level.SEVERE, null, ex);
            }
        }
        //______________________________________________________________________
        if (name.equals("order")) {
            ListOrderJAXB lsjaxb = new ListOrderJAXB();
            try {
                InitialContext ic = new InitialContext();
                DataSource ds = (DataSource) ic.lookup("jdbc/laba_3");
                Connection conn = ds.getConnection();
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery("select * from laba_3.order where " + " phone = " + phone);

                while (rs.next()) {
                    //с 1 до n id phone name info
                    lsjaxb.add(new OrderJAXB(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4),rs.getString(5)));
                }

            } catch (Exception ex) {

            }
            try {
                
                JAXBContext context = JAXBContext.newInstance(ListOrderJAXB.class);
                Marshaller marshaller = context.createMarshaller();
                 StringWriter sw = new StringWriter();                
                marshaller.marshal(lsjaxb, sw);                
                return sw.toString();
            } catch (JAXBException ex) {
                Logger.getLogger(StreamXMLBean.class.getName())
                        .log(Level.SEVERE, null, ex);
            }
        }
        //______________________________________________________________________
        return null;
    }

    @Override
    public void read(String xml, String name) {

        if (name.equals("user")) {
            try {
                StringReader reader = new StringReader(xml);
                JAXBContext context = JAXBContext.newInstance(UserJAXB.class);
                Unmarshaller unmarshaller = context.createUnmarshaller();
                UserJAXB user = (UserJAXB) unmarshaller.unmarshal(reader);

                InitialContext ic = new InitialContext();
                DataSource ds = (DataSource) ic.lookup("jdbc/laba_3");
                Connection conn = ds.getConnection();

                Statement stmt = conn.createStatement();
                stmt.execute("INSERT INTO `laba_3`.`user` (`phone`, `password`) VALUES ('" + user.getPhone() + "', '" + user.getPassword() + "')");

            } catch (JAXBException ex) {
                Logger.getLogger(StreamXMLBean.class.getName())
                        .log(Level.SEVERE, null, ex);
            } catch (Exception e) {
                System.out.println("error");
            }
        }
        //______________________________________________________________________
        if (name.equals("service")) {
            try {
                StringReader reader = new StringReader(xml);
                JAXBContext context = JAXBContext.newInstance(ListServiceJAXB.class);
                Unmarshaller unmarshaller = context.createUnmarshaller();
                ListServiceJAXB lsjaxb = (ListServiceJAXB) unmarshaller.unmarshal(reader);

                InitialContext ic = new InitialContext();
                DataSource ds = (DataSource) ic.lookup("jdbc/laba_3");
                Connection conn = ds.getConnection();

                Statement stmt = conn.createStatement();
                ArrayList<ServiceJAXB> arrayList = lsjaxb.getArrayList();
                for (ServiceJAXB serviceJAXB : arrayList) {
                    stmt.execute("INSERT INTO `laba_3`.`service` (`id`, `phone`, `name`, `info`) VALUES ('" + serviceJAXB.getId() + "', '" + serviceJAXB.getPhone() + "', '" + serviceJAXB.getName() + "', '" + serviceJAXB.getInfo() + "')");
                }

            } catch (JAXBException ex) {
                Logger.getLogger(StreamXMLBean.class.getName())
                        .log(Level.SEVERE, null, ex);
            } catch (Exception e) {
                System.out.println("error");
            }
        }
         //______________________________________________________________________
        if (name.equals("order")) {
            try {
                StringReader reader = new StringReader(xml);
                JAXBContext context = JAXBContext.newInstance(ListOrderJAXB.class);
                Unmarshaller unmarshaller = context.createUnmarshaller();
                ListOrderJAXB lsjaxb = (ListOrderJAXB) unmarshaller.unmarshal(reader);

                InitialContext ic = new InitialContext();
                DataSource ds = (DataSource) ic.lookup("jdbc/laba_3");
                Connection conn = ds.getConnection();

                Statement stmt = conn.createStatement();
                ArrayList<OrderJAXB> arrayList = lsjaxb.getArrayList();
                for (OrderJAXB orderJAXB : arrayList) {
                    stmt.execute("INSERT INTO `laba_3`.`order` (`id`, `phone`, `name`, `info`, `status`) VALUES ('"+ orderJAXB.getId()+"', '"+orderJAXB.getPhone() +"', '"+orderJAXB.getName() +"', '"+ orderJAXB.getInfo()+"', '"+orderJAXB.getStatus() +"')");
                }

            } catch (JAXBException ex) {
                Logger.getLogger(StreamXMLBean.class.getName())
                        .log(Level.SEVERE, null, ex);
            } catch (Exception e) {
                System.out.println("error");
            }
        }

    }

}
