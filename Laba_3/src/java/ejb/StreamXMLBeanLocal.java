/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import javax.ejb.Local;

/**
 *
 * @author Vladisalv
 */


@Local
public interface StreamXMLBeanLocal {
 
    public String write(String name, String phone);
    
    public void read(String xml, String name);
    
}
