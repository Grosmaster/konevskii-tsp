/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dto;

/**
 *
 * @author Vladisalv
 */
public class OrderDto {
    
    private int number;
    private String phone;
    private String name;
    private String info;
    private String status;

    public OrderDto(int number, String phone, String name, String info, String status) {
        this.number = number;
        this.phone = phone;
        this.name = name;
        this.info = info;
        this.status = status;
    }

    public int getNumber() {
        return number;
    }

    public String getPhone() {
        return phone;
    }

    public String getName() {
        return name;
    }

    public String getInfo() {
        return info;
    }

    public String getStatus() {
        return status;
    }    
    
}
