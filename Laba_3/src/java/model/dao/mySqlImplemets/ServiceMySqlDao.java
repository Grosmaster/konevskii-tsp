/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao.mySqlImplemets;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import model.dao.ServiceDao;
import model.dto.ServiceDto;

/**
 *
 * @author Vladisalv
 */
public class ServiceMySqlDao implements ServiceDao{

    @Override
    public  List<ServiceDto> services(int phone) {
         try
       {
        InitialContext ic = new InitialContext();
        DataSource ds = (DataSource) ic.lookup("jdbc/laba_3");
        Connection conn = ds.getConnection();
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("select * from laba_3.service where " + " phone = " + phone);
        List<ServiceDto> list = new ArrayList<>();

        while(rs.next()){
            
            list.add(new ServiceDto(rs.getString(3), rs.getString(4)));

        }      

        
        return list;
       }
       catch(Exception ex)
       {
           return null;
       }       
    }

    @Override
    public void servicAdd(int phone, String name, String info) {
         try
       {
        InitialContext ic = new InitialContext();
        DataSource ds = (DataSource) ic.lookup("jdbc/laba_3");
        Connection conn = ds.getConnection();
        
        Statement stmt = conn.createStatement();
        stmt.execute("INSERT INTO `laba_3`.`service` (`phone`, `name`, `info`) VALUES ('"+phone+"', '"+name+"', '"+info+"')");
       }
       catch(Exception ex)
       {
    
       }       
    }
    
}
