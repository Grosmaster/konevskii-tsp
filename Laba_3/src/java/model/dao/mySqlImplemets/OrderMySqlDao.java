/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao.mySqlImplemets;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import model.dao.OrderDao;
import model.dto.OrderDto;
import model.dto.ServiceDto;

/**
 *
 * @author Vladisalv
 */
public class OrderMySqlDao implements OrderDao {

    @Override
    public List<OrderDto> orders() {
        try {
            InitialContext ic = new InitialContext();
            DataSource ds = (DataSource) ic.lookup("jdbc/laba_3");
            Connection conn = ds.getConnection();
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select * from laba_3.order");
            List<OrderDto> list = new ArrayList<>();

            while (rs.next()) {
                list.add(new OrderDto(Integer.parseInt(rs.getString(1)), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5)));

            }
            return list;
        } catch (Exception ex) {
            return null;
        }
    }

    @Override
    public void orderAdd(int phone, String name, String info, String status) {
        try {
            InitialContext ic = new InitialContext();
            DataSource ds = (DataSource) ic.lookup("jdbc/laba_3");
            Connection conn = ds.getConnection();

            Statement stmt = conn.createStatement();
            stmt.execute("INSERT INTO `laba_3`.`order` (`phone`, `name`, `info`, `status`) VALUES ('" + phone + "', '" + name + "', '" + info + "', '" + status + "')");
        } catch (Exception ex) {

        }
    }

    @Override
    public void orderUpdate(int number, String status) {
        try {
            InitialContext ic = new InitialContext();
            DataSource ds = (DataSource) ic.lookup("jdbc/laba_3");
            Connection conn = ds.getConnection();

            Statement stmt = conn.createStatement();
            stmt.executeUpdate("UPDATE `laba_3`.`order` SET `status`='" + status + "' WHERE `id`='" + number + "';");
        } catch (Exception ex) {

        }
    }

    @Override
    public OrderDto order(int number) {
        try {
            InitialContext ic = new InitialContext();
            DataSource ds = (DataSource) ic.lookup("jdbc/laba_3");
            Connection conn = ds.getConnection();
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select * from laba_3.order where " + " id = " + number);
            OrderDto order = null;
            while (rs.next()) {
                order = new OrderDto(Integer.parseInt(rs.getString(1)), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5));
            }
            return order;
        } catch (Exception ex) {
            return null;
        }
    }

}
