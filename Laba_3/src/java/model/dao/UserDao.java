/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.util.List;

/**
 *
 * @author Vladisalv
 */
public interface UserDao {
    
    void createUser(int phone, String password);
    
    void updatePassword(String phone, String password);
    
    String loginUser(int phone);
    
    List<String> phones();
    
}
