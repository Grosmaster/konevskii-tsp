/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao.hibernate;

import entity.HibernateUtil;
import entity.Service;
import entity.User;
import java.util.ArrayList;
import java.util.List;
import model.dao.ServiceDao;
import model.dto.OrderDto;
import model.dto.ServiceDto;

/**
 *
 * @author Vladisalv
 */
public class ServiceHibernateDao implements ServiceDao {

    @Override
    public List<ServiceDto> services(int phone) {
        List<Service> list = HibernateUtil.getSessionFactory().openSession().createQuery("from Service where phone = " + phone).list();
        List<ServiceDto> arr = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            arr.add(new ServiceDto(list.get(i).getName(), list.get(i).getInfo()));
        }
        return arr;
    }

    @Override
    public void servicAdd(int phone, String name, String info) {
        HibernateUtil.getSessionFactory().openSession().createSQLQuery("INSERT INTO `laba_3`.`service` (`phone`, `name`, `info`) VALUES ('" + phone + "', '" + name + "', '" + info + "')").executeUpdate();
    }

}
