/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao.hibernate;

import entity.HibernateUtil;
import entity.Order;
import entity.User;
import java.util.ArrayList;
import java.util.List;
import model.dao.OrderDao;
import model.dto.OrderDto;

/**
 *
 * @author Vladisalv
 */
public class OrderHibernateDao implements OrderDao{

    @Override
    public List<OrderDto> orders() {
        List<Order> list = HibernateUtil.getSessionFactory().openSession().createCriteria(Order.class).list();
        List<OrderDto> arr = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            arr.add(new OrderDto(list.get(i).getId(), String.valueOf(list.get(i).getUser().getPhone()), list.get(i).getName(), list.get(i).getInfo(), list.get(i).getStatus()));
        }
        return arr;
    }

    @Override
    public void orderAdd(int phone, String name, String info, String status) {
         HibernateUtil.getSessionFactory().openSession().createSQLQuery("INSERT INTO `laba_3`.`order` (`phone`, `name`, `info`, `status`) VALUES ('" + phone + "', '" + name + "', '" + info + "', '" + status + "')").executeUpdate();
    }

    @Override
    public void orderUpdate(int number, String status) {
        HibernateUtil.getSessionFactory().openSession().createSQLQuery("UPDATE `laba_3`.`order` SET `status`='" + status + "' WHERE `id`='" + number + "';").executeUpdate();
    }

    @Override
    public OrderDto order(int number) {
        List<Order> list = HibernateUtil.getSessionFactory().openSession().createQuery("from Order where id = " + number).list();
        int i = 0;
        return new OrderDto(list.get(i).getId(), String.valueOf(list.get(i).getUser().getPhone()), list.get(i).getName(), list.get(i).getInfo(), list.get(i).getStatus());
    }
    
}
