/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao.hibernate;

import entity.HibernateUtil;
import entity.User;
import java.util.ArrayList;
import java.util.List;
import javax.xml.datatype.DatatypeFactory;
import model.dao.UserDao;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Vladisalv
 */
public class UserHibernateDao implements UserDao {

    @Override
    public void createUser(int phone, String password) {
        //HibernateUtil.getSessionFactory().openSession().createQuery("insert into User("+phone+", "+password+") select phone,password from user").executeUpdate();
        HibernateUtil.getSessionFactory().openSession().createSQLQuery("INSERT INTO `laba_3`.`user` (`phone`, `password`) VALUES ('" + phone + "', '" + password + "')").executeUpdate();

    }

    @Override
    public void updatePassword(String phone, String password) {
        HibernateUtil.getSessionFactory().openSession().createSQLQuery("UPDATE `laba_3`.`user` SET `password`='" + password + "' WHERE `phone`='" + phone + "';").executeUpdate();
    }

    @Override
    public String loginUser(int phone) {
        List<User> list = HibernateUtil.getSessionFactory().openSession().createQuery("from User where phone = " + phone).list();

        return list.get(0).getPassword();
    }

    @Override
    public List<String> phones() {

        List<User> list = HibernateUtil.getSessionFactory().openSession().createCriteria(User.class).list();
        List<String> arr = new ArrayList<>();
        for (User user : list) {
            arr.add(String.valueOf(user.getPhone()));
        }
        return arr;

        //return (List<String>) HibernateUtil.getSessionFactory().openSession().createCriteria(User.class).setProjection(Projections.property("phone")).uniqueResult();
    }

}
