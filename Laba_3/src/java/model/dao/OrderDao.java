/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.util.List;
import model.dto.OrderDto;
import model.dto.ServiceDto;

/**
 *
 * @author Vladisalv
 */
public interface OrderDao {
    
    List<OrderDto> orders();
    void orderAdd(int phone, String name, String info,String Status);
    void orderUpdate(int number, String status);
    OrderDto order(int number);
    
}
