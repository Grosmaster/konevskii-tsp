/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.util.List;
import model.dto.ServiceDto;

/**
 *
 * @author Vladisalv
 */
public interface ServiceDao {
    
    List<ServiceDto> services(int number);
    
    void servicAdd(int phone, String name, String info);
    
}
