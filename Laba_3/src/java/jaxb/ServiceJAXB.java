package jaxb;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ServiceJAXB {

    private String id;
    private String phone;
    private String name;
    private String info;

    public ServiceJAXB() {
    }

    public ServiceJAXB(String id, String phone, String name, String info) {
        this.id = id;
        this.phone = phone;
        this.name = name;
        this.info = info;
    }

    public String getId() {
        return id;
    }

    @XmlElement
    public void setId(String id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    @XmlElement
    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    @XmlElement
    public void setName(String name) {
        this.name = name;
    }

    public String getInfo() {
        return info;
    }

    @XmlElement
    public void setInfo(String info) {
        this.info = info;
    }

    @Override
    public String toString() {
        return "ServiceJAXB{"
                + "id='" + id + '\''
                + ", phone='" + phone + '\''
                + ", name='" + name + '\''
                + ", info='" + info + '\''
                + '}';
    }
}
