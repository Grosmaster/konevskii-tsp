package jaxb;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;

@XmlRootElement
public class ListServiceJAXB {

    ArrayList<ServiceJAXB> arrayList;

    public ListServiceJAXB() {
        this.arrayList = new ArrayList<>();
    }

    public ArrayList<ServiceJAXB> getArrayList() {
        return arrayList;
    }

    @XmlElement
    public void setArrayList(ArrayList<ServiceJAXB> arrayList) {
        this.arrayList = arrayList;
    }

    public void add(ServiceJAXB serviceJAXB) {
        arrayList.add(serviceJAXB);
    }

    @Override
    public String toString() {
        return "ListServiceJAXB{"
                + "arrayList=" + arrayList
                + '}';
    }
}
