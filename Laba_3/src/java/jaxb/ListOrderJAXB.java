package jaxb;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
@XmlRootElement
public class ListOrderJAXB {

    ArrayList<OrderJAXB> arrayList;

    public ListOrderJAXB() {
        this.arrayList = new ArrayList<>();
    }

    public ArrayList<OrderJAXB> getArrayList() {
        return arrayList;
    }

    @XmlElement
    public void setArrayList(ArrayList<OrderJAXB> arrayList) {
        this.arrayList = arrayList;
    }

    public void add(OrderJAXB orderJAXB)
    {
        arrayList.add(orderJAXB);
    }

    @Override
    public String toString() {
        return "ListOrderJAXB{" +
                "arrayList=" + arrayList +
                '}';
    }
}
