package jaxb;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class OrderJAXB {

    private String id;
    private String phone;
    private String name;
    private String info;
    private String status;

    public OrderJAXB() {
    }

    public OrderJAXB(String id, String phone, String info, String name, String status) {
        this.id = id;
        this.phone = phone;
        this.info = info;
        this.name = name;
        this.status = status;
    }

    public String getId() {
        return id;
    }
    @XmlElement
    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    @XmlElement
    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }
    @XmlElement
    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getInfo() {
        return info;
    }
    @XmlElement
    public void setInfo(String info) {
        this.info = info;
    }

    public String getStatus() {
        return status;
    }
    @XmlElement
    public void setStatus(String status) {
        this.status = status;
    }
}
